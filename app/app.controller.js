angular.module('groupsApp')
.controller('groupCtrl', function($uibModal, $scope){

    $scope.groups = {
        "Private": [],
        "Public": []
    };

    $scope.typesGroup = ["Public","Private"]
    $scope.typeNewGroup = "";
    $scope.newGroupName = "";
    var modalInstance;

    $scope.openModal = function(){
         modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/modal.html',
            scope: $scope
        });
    };

    $scope.closeModal = function(){
        modalInstance.close();
        $scope.newGroupName = "";
    };


    $scope.createGroup = function(newGroupName, typeNewGroup){
        $scope.invalidData = false;
        var matchVar = newGroupName.match(/^[^0-9][a-zA-Z0-9\-]+$/g);
        if(matchVar && matchVar.length){
            $scope.groups[typeNewGroup].push("Group " +newGroupName);
            modalInstance.close();
        }else{
            $scope.invalidData = true;
        }

    };

});